import React from "react";
import BotaoGol from "./BotaoGol";

const Time = ({ nome, gols, marcarGol }) => (
  <React.Fragment>
    <h1>{nome}</h1>
    <h2>{gols}</h2>
    <BotaoGol marcarGol={marcarGol} />
  </React.Fragment>
);

// export default class Time extends React.Component {
//     render(){
//         return(
//             <React.Fragment>
//                 <div>
//                     <h1>{this.props.nome}</h1>
//                     <h2>{this.props.gols}</h2>
//                     <BotaoGol marcarGol={this.props.marcarGol}/>
//                 </div>
//             </React.Fragment>
//         );
//     }
// }

export default Time;