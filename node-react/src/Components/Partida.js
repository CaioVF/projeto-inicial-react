import React from 'react';

const Partida = ({estadio, data, horario}) => (
    <React.Fragment>
        <div>
            <h2>{estadio}</h2>
        </div>
        <div>
            <span>{data}</span>
            <span>-</span>
            <span>{horario}</span>
        </div>
    </React.Fragment>
);


// class Partida extends React.Component {

//     render(){
//         return(
//             <React.Fragment>
//                 <div>
//                     <h2>{this.props.estadio}</h2>
//                 </div>
//                 <div>
//                     <span>{this.props.data}</span>
//                     <span> - </span>
//                     <span>{this.props.horario}</span>
//                 </div>
//             </React.Fragment>
//         );
//     }
// }

export default Partida;